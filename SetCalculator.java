import java.util.Scanner;
import java.util.TreeSet;

public class SetCalculator {
  private static Scanner in = new Scanner(System.in);
  private static String readInput() {
    String s = in.nextLine();
    return s;
  }

  private static TreeSet<Integer> getUnion(TreeSet<Integer> s1, TreeSet<Integer> s2) {
    TreeSet<Integer> result = new TreeSet<Integer>(s1);
    result.addAll(s2);
    return result;
  }

  private static TreeSet<Integer> getIntersection(TreeSet<Integer> s1, TreeSet<Integer> s2) {
    TreeSet<Integer> result = new TreeSet<Integer>(s1);
    result.retainAll(s2);
    return result;
  }

  private static TreeSet<Integer> getDifference(TreeSet<Integer> s1, TreeSet<Integer> s2) {
    TreeSet<Integer> result = new TreeSet<Integer>(s1);
    result.removeAll(s2);
    return result;
  }

  private static void executeSetOperation(TreeSet<Integer> s1, TreeSet<Integer> s2, String operator) {
    TreeSet<Integer> result = new TreeSet<Integer>();
    switch(operator){
      case "+": result = getUnion(s1, s2); break;
      case "-": result = getDifference(s1, s2); break;
      case "*": result = getIntersection(s1, s2); break;
    }
    System.out.println(result);
  }

  private static void shutdown() {
    in.close();
    System.exit(0);
  }

  public static void main(String[] args) {
    String usageRegex = "\\[(\\d+,?)*\\][\\+\\-\\*]\\[(\\d+,?)*\\]";

    while(true) {
      String input = readInput();
      if (input.isEmpty()) {
        shutdown();
      }
      input = input.replaceAll("\\s","");
      if(!input.matches(usageRegex)) {
        System.out.println("Usage: <set><operator><set> e.g. [1,2,3]+[2,3,4]");
        continue;
      }

      TreeSet<Integer> s1 = new TreeSet<Integer>();
      TreeSet<Integer> s2 = new TreeSet<Integer>();
      String operator = "";
      String numberStr = "";
      boolean firstNumber = true;

      for(char c: input.toCharArray()) {
        if (c == '[') {
          continue;
        }
        if (c == ',' || c == ']') {
          if (numberStr.length() > 0) {
            int number = Integer.parseInt(numberStr);
            if(firstNumber) {
              s1.add(number);
            } else {
              s2.add(number);
            }
            numberStr = "";
          }
          continue;
        }
        if (c == '+' || c == '-' || c == '*') {
          firstNumber = false;
          operator = String.valueOf(c);
          continue;
        }
        numberStr += c;
      }

      executeSetOperation(s1, s2, operator);
    }
  }
}
